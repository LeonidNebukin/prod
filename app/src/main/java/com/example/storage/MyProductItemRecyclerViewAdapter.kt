package com.example.storage

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.storage.entity.Product

class MyProductItemRecyclerViewAdapter(private val clickListener: (Product) -> Unit) :
    RecyclerView.Adapter<MyProductItemRecyclerViewAdapter.ViewHolder>() {
    private var values: List<Product> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_recycler_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.id.toString()
        holder.nameView.text = item.name
        holder.costProduct.text = "${item.cost}$"
        holder.itemView.setOnClickListener { clickListener(item) }
    }

    override fun getItemCount(): Int = values.size

    fun setProduct(list : List<Product>) {
        values = list
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView: TextView = view.findViewById(R.id.product_id)
        val nameView: TextView = view.findViewById(R.id.product_name)
        val costProduct: TextView = view.findViewById(R.id.product_cost)
    }
}
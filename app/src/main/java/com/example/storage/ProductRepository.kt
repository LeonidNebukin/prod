package com.example.storage

import androidx.lifecycle.LiveData
import com.example.storage.dao.ProductDAO
import com.example.storage.entity.Product

// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class ProductRepository(private val productDAO: ProductDAO) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allProducts: LiveData<List<Product>> = productDAO.getProductList()

    suspend fun insert(product: Product) {
        productDAO.insert(product)
    }

    suspend fun insertOrReplace(product: Product) {
        productDAO.insertOrReplace(product)
    }
}
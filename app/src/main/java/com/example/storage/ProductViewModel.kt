package com.example.storage

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.storage.entity.Product
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ProductRepository
    val allWords: LiveData<List<Product>>

    init {
        val wordsDao = RoomModule.getDatabase(application).productDao()
        repository = ProductRepository(wordsDao)
        allWords = repository.allProducts
    }

    fun insert(product: Product) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(product)
    }

    fun insertOrReplace(product: Product) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertOrReplace(product)
    }
}
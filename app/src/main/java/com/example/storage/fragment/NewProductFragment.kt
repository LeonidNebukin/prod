package com.example.storage.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.storage.ProductViewModel
import com.example.storage.R
import com.example.storage.entity.Product
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_new_product.*
import kotlinx.android.synthetic.main.fragment_recycler_view.*

class NewProductFragment(val product: Product? = null) : Fragment() {

    private lateinit var productViewModel: ProductViewModel
    private val editMode : Boolean = product != null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_new_product, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        product?.let {
            nameProd.setText(product?.name)
            costProd.setText(product?.cost.toString())
        }
        activity?.fab?.setOnClickListener {
            if (!nameProd.text.isNullOrEmpty() && !costProd.text.isNullOrEmpty()) {
                if (editMode) {
                    productViewModel.insertOrReplace(
                        Product(
                            id = product?.id ?: 0,
                            name = nameProd.text.toString(),
                            cost = costProd.text.toString().toDouble()
                        )
                    )
                }
                else
                {
                    productViewModel.insert(
                        Product(
                            name = nameProd.text.toString(),
                            cost = costProd.text.toString().toDouble()
                        )
                    )
                }
            }
            activity?.onBackPressed()
        }
        activity?.fab?.setImageResource(R.drawable.ic_baseline_check_24)
    }

    override fun toString(): String {
        return super.toString()
    }
}
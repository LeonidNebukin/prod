package com.example.storage.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.storage.MyProductItemRecyclerViewAdapter
import com.example.storage.ProductViewModel
import com.example.storage.R
import com.example.storage.entity.Product
import kotlinx.android.synthetic.main.activity_main.*

/**
 * A fragment representing a list of Items.
 */
class ProductListFragment : Fragment() {
    private lateinit var productAdapter : MyProductItemRecyclerViewAdapter
    private lateinit var productViewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productAdapter = MyProductItemRecyclerViewAdapter { product ->
            itemClick(product = product)
        }
        productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)
        productViewModel.allWords.observe(this, Observer { products ->
            // Update the cached copy of the words in the adapter.
            products?.let { productAdapter.setProduct(it) }
        })
    }

    private fun itemClick(product : Product) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.container, NewProductFragment(product))
            ?.addToBackStack(null)
            ?.commit()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_recycler_view_list, container, false)

        var recyclerView = view.findViewById<RecyclerView>(R.id.list)
        // Set the adapter
        if (recyclerView is RecyclerView) {
            with(recyclerView) {
                layoutManager = LinearLayoutManager(context)
                adapter = productAdapter
            }
        }
        activity?.fab?.setOnClickListener {
            activity?.
                supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.container, NewProductFragment())
                ?.addToBackStack(null)
                ?.commit()
        }
        activity?.fab?.setImageResource(R.drawable.ic_baseline_add_24)
        return view
    }

    override fun toString(): String {
        return super.toString()
    }
}
package com.example.storage.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "product_table")
data class Product(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "productId")  val id : Int = 0,
    @ColumnInfo(name = "name") val name : String,
    @ColumnInfo(name = "cost") val cost : Double
)